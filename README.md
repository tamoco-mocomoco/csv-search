# 概要

CSVを検索するウェブシステムです。`index.html`だけで作られています。

# 使っているフレームワーク

- JavaScript：Vue.js
- CSV変換：PapaParse
- CSS：bulma

# 使い方

- カラムを指定して検索することができます
- 数値のみのデータである場合はオプションで数値の比較で抽出できます
- XMLHttpRrequest予めCSVのデータを読み込むことができます